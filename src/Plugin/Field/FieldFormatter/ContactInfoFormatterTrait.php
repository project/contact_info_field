<?php

namespace Drupal\contact_info_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Render\Markup;

trait ContactInfoFormatterTrait {

  protected function getItemValues(FieldItemInterface $item) {
    $userLink = null;
    if ($item->uid) {
      $user = User::load($item->uid);
      $userLink = Markup::create($user->toLink());
    }
    $mail = $item->email ? Markup::create('<a href="mailto:' . $item->email . '">' . $item->email . '</a>') : null;
    $url = $item->url ? Markup::create('<a href="' . $item->url . '">' . $item->url . '</a>') : null;

    $values = [
      'Name' => $item->name,
      'Position' => $item->position,
      'E-mail' => $mail,
      'Phone' => $item->phone,
      'User account' => $userLink,
      'Link' => $url,
      'Notes' => $item->notes,
    ];
    // $values = array_filter($values);
    return $values;
  }
}
