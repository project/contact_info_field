<?php

namespace Drupal\contact_info_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'contact_info_field' widget.
 *
 * @FieldWidget (
 *   id = "contact_info_field",
 *   label = @Translation("Contact info widget"),
 *   field_types = {
 *     "contact_info_field"
 *   }
 * )
 */
class ContactInfoFieldWidget extends WidgetBase
{
  const elements = [
    'name' => [
      '#type' => 'textfield',
      '#title' => 'Contact name',
      '#required' => FALSE,
      '#show_by_default' => TRUE
    ],
    'position' => [
      '#type' => 'textfield',
      '#title' => 'Position/role',
      '#required' => FALSE,
      '#show_by_default' => FALSE
    ],
    'email' => [
      '#type' => 'email',
      '#title' => 'E-mail',
      '#required' => FALSE,
      '#show_by_default' => TRUE
    ],
    'phone' => [
      '#type' => 'textfield',
      '#title' => 'Phone number',
      '#required' => false,
      '#show_by_default' => TRUE
    ],
    'url' => [
      '#type' => 'uri',
      '#title' => 'URL',
      '#required' => FALSE,
      '#show_by_default' => FALSE
    ],
    'notes' => [
      '#type' => 'textarea',
      '#title' => 'Notes',
      '#required' => FALSE,
      '#show_by_default' => FALSE
    ],
    'uid' => [
      '#type' => 'entity_autocomplete',
      '#title' => 'User reference',
      '#target_type' => 'user',
      '#selection_settings' => [
        'include_anonymous' => FALSE,
      ],
      '#tags' => FALSE,
      '#required' => FALSE,
      '#maxlength' => NULL,
      '#show_by_default' => FALSE
    ]
  ];

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings()
  {
    $show = [];
    foreach (static::elements as $id => $data) {
      $show['show_' . $id] = $data['#show_by_default'];
    }
    return $show + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state)
  {
    $elements = parent::settingsForm($form, $form_state);

    foreach (static::elements as $id => $data) {
      $elements['show_' . $id] = [
        '#type' => 'checkbox',
        '#title' => $this->t("Enable '" . $data['#title'] . "'"),
        '#default_value' => $this->getSetting('show_' . $id) ?? true
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary()
  {
    $summary = [];

    foreach (static::elements as $id => $data) {
      $summary[] = t('@name: @show', ['@name' => $data['#title'], '@show' => $this->getSetting('show_' . $id) ? 'available' : 'hidden']);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state)
  {
    $entity = $items->getEntity();

    foreach (static::elements as $id => $data) {
      if ($this->getSetting('show_' . $id)) {
        $element[$id] = $data;
        $element[$id]['#title'] = $this->t($element[$id]['#title']);
        $element[$id]['#default_value'] = isset($items[$delta]->$id) ? $items[$delta]->$id : null;
      }
    }

    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a details element.
    if ($this->fieldDefinition->getFieldStorageDefinition()->getCardinality() == 1) {
      $element += array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('container-inline')),
      );
    }

    return $element;
  }
}
